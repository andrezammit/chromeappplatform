// ChromeAppPlatformDlg.cpp : implementation file
//

#include "stdafx.h"

#include "ChromeAppPlatform.h"
#include "ChromeAppPlatformDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define TRAY_ICON_ID			1001
#define TIMER_UPDATE			1001

#define UPDATE_TIMEOUT			500

#define STD_OUT_BUFFER_SIZE		4096

// CChromeAppPlatformDlg dialog

CChromeAppPlatformDlg::CChromeAppPlatformDlg(CWnd* pParent /*= NULL*/)
	: CDialogEx(IDD_CHROMEAPPPLATFORM_DIALOG, pParent)
{
	m_isTopMost = false;
	m_isOverlayWindow = false;

	m_shellTrayWnd = NULL;
	m_cachedForegroundWnd = NULL;

	m_iconHandle = LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME));
	m_trayIconHandle = m_iconHandle;
}

CChromeAppPlatformDlg::~CChromeAppPlatformDlg()
{
	DestroyIcon(m_iconHandle);
	DestroyIcon(m_trayIconHandle);
}

void CChromeAppPlatformDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CChromeAppPlatformDlg, CDialogEx)
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_WM_CLOSE()

	ON_COMMAND(ID_CHROMEAPPPLATFORM_CLOSE, OnTrayClose)

	ON_COMMAND_RANGE(WM_TRAY_START, WM_TRAY_END, OnTrayItemClicked)

	ON_MESSAGE(WM_TRAY_NOTIF, OnTrayNotif)
	ON_MESSAGE(WM_PROCESS_CLOSED, OnProcessClosed)
	ON_MESSAGE(WM_EXPROCESS_OUTPUT, OnExtProcessOutput)
	ON_MESSAGE(WM_WINDOW_LOADED_NOTIF, OnWindowLoadedNotif)
END_MESSAGE_MAP()

// CChromeAppPlatformDlg message handlers

BOOL CChromeAppPlatformDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_shellTrayWnd = ::FindWindow(_T("Shell_TrayWnd"), NULL);

	SetWindowProperties();
	CreateTrayIcon();

	CRect rect;
	GetClientRect(&rect);

	auto url = theApp.m_manifest["url"].get<string>();
	m_browserInterface.Create(GetSafeHwnd(), rect, url.c_str());

	return TRUE;
}

void CChromeAppPlatformDlg::SetWindowProperties()
{
	auto titleItem = theApp.m_manifest["title"];

	if (!titleItem.is_null())
	{
		CString title(titleItem.get<string>().c_str());
		SetWindowText(title);
	}

	auto opacityItem = theApp.m_manifest["opacity"];

	if (!opacityItem.is_null())
	{
		int opacity = opacityItem;

		if (opacity < 255)
		{
			ModifyStyleEx(0, WS_EX_LAYERED);
			SetLayeredWindowAttributes(RGB(1, 1, 1), opacity, LWA_ALPHA | LWA_COLORKEY);
		}
	}

	CRect rect;
	GetWindowRect(rect);

	auto widthItem = theApp.m_manifest["width"];

	if (!widthItem.is_null())
	{
		int width = widthItem;
		rect.right = rect.left + width;
	}

	auto heightItem = theApp.m_manifest["height"];

	if (!heightItem.is_null())
	{
		int height = heightItem;
		rect.bottom = rect.top + height;
	}

	MoveWindow(rect);

	auto maximizedItem = theApp.m_manifest["maximized"];

	if (!maximizedItem.is_null())
	{
		bool maximized = maximizedItem;

		if (maximized)
		{
			ShowWindow(SW_MAXIMIZE);
		}
	}

	auto overlayItem = theApp.m_manifest["overlay"];

	if (!overlayItem.is_null())
	{
		bool overlay = overlayItem;

		if (overlay)
		{
			m_isOverlayWindow = true;

			ModifyStyle(WS_OVERLAPPEDWINDOW, 0);
			ModifyStyleEx(WS_EX_APPWINDOW | WS_EX_DLGMODALFRAME | WS_EX_CLIENTEDGE, WS_EX_TRANSPARENT);

			ResizeWindowToCoverDesktop();
		}
	}

	auto iconItem = theApp.m_manifest["icon"];

	if (!iconItem.is_null())
	{
		CString iconPath(iconItem.get<string>().c_str());

		m_iconHandle = (HICON) ::LoadImage(NULL, iconPath, IMAGE_ICON, 0, 0, LR_LOADFROMFILE);
		m_trayIconHandle = (HICON) ::LoadImage(NULL, iconPath, IMAGE_ICON, 16, 16, LR_LOADFROMFILE);

		SetIcon(m_iconHandle, TRUE);
	}
}

void CChromeAppPlatformDlg::ResizeWindowToCoverDesktop()
{
	CWnd* pWnd = GetDesktopWindow();

	CRect rect;
	pWnd->GetWindowRect(rect);

	rect.bottom -= 1;

	MoveWindow(rect);
}

void CChromeAppPlatformDlg::OnClose()
{
	for (auto process : m_procMonitorThreads)
	{
		shared_ptr<CExternalProcess> pExtProc = process.second;
		TerminateProcess(pExtProc->m_processHandle, 0);

		if (pExtProc->m_monitorThread.joinable())
		{
			pExtProc->m_monitorThread.join();
		}

		if (pExtProc->m_outputRedirectThread.joinable())
		{
			pExtProc->m_outputRedirectThread.join();
		}
	}

	if (!m_browserInterface.IsReadyToClose())
	{
		m_browserInterface.PrepareToClose();
		return;
	}

	RemoveTrayIcon();

	CDialogEx::OnClose();
}

void CChromeAppPlatformDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	CRect rect;
	GetClientRect(&rect);

	m_browserInterface.UpdateSize(rect);
}

void CChromeAppPlatformDlg::OnTimer(UINT_PTR uIDEvent)
{
	if (m_isOverlayWindow)
	{
		ForegroundWindowCheck();
	}
}

void CChromeAppPlatformDlg::ForegroundWindowCheck()
{
	HWND foregroundWnd = ::GetForegroundWindow();

	if (foregroundWnd == GetSafeHwnd())
	{
		return;
	}

	if (m_cachedForegroundWnd == foregroundWnd)
	{
		return;
	}

	m_cachedForegroundWnd = foregroundWnd;

	HWND insertAfterWnd = NULL;

	if (foregroundWnd == m_shellTrayWnd)
	{
		insertAfterWnd = HWND_NOTOPMOST;
	}
	else
	{
		CString rClassName;

		GetClassName(foregroundWnd, rClassName.GetBuffer(512), 512);
		rClassName.ReleaseBuffer();

		if (rClassName == _T("WorkerW") &&
			::GetParent(foregroundWnd) == NULL)
		{
			m_isTopMost = true;
			insertAfterWnd = m_shellTrayWnd;
		}
		else 
		{
			m_isTopMost = false;
			insertAfterWnd = HWND_BOTTOM;
		}
	}

	CRect rect;
	GetClientRect(&rect);

	::SetWindowPos(GetSafeHwnd(), insertAfterWnd, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_NOACTIVATE);
}

LRESULT CChromeAppPlatformDlg::OnWindowLoadedNotif(WPARAM wParam, LPARAM lParam)
{
	SetTimer(TIMER_UPDATE, UPDATE_TIMEOUT, NULL);

	RunExternalApp();

	return 0;
}

void CChromeAppPlatformDlg::CreateTrayIcon()
{
	auto tray = theApp.m_manifest["tray"];

	if (tray.is_null())
	{
		return;
	}

	NOTIFYICONDATA notifIconData = { 0 };

	notifIconData.uID = TRAY_ICON_ID;
	notifIconData.hWnd = GetSafeHwnd();
	notifIconData.cbSize = sizeof(notifIconData);
	notifIconData.uCallbackMessage = WM_TRAY_NOTIF;
	notifIconData.uFlags = NIF_ICON | NIF_TIP | NIF_MESSAGE;
	notifIconData.hIcon = m_trayIconHandle;

	auto titleItem = theApp.m_manifest["title"];

	if (!titleItem.is_null())
	{
		CString title(titleItem.get<string>().c_str());
		_tcscpy_s(notifIconData.szTip, title);
	}

	Shell_NotifyIcon(NIM_ADD, &notifIconData);
}

void CChromeAppPlatformDlg::RemoveTrayIcon()
{
	NOTIFYICONDATA notifIconData = { 0 };

	notifIconData.uID = TRAY_ICON_ID;
	notifIconData.cbSize = sizeof(notifIconData);

	Shell_NotifyIcon(NIM_DELETE, &notifIconData);
}

LRESULT CChromeAppPlatformDlg::OnTrayNotif(WPARAM wParam, LPARAM lParam)
{
	switch (LOWORD(lParam))
	{
	case WM_RBUTTONDOWN:
	case WM_CONTEXTMENU:
		{
			CPoint pt;
			GetCursorPos(&pt);

			ShowTrayContextMenu(pt);
		}
		break;
	}
	
	return 0;
}

void CChromeAppPlatformDlg::OnTrayItemClicked(UINT command)
{
	CString& jsFunction = m_trayCommands[command];
	m_browserInterface.ExecuteJavaScript(jsFunction);
}

void CChromeAppPlatformDlg::ShowTrayContextMenu(const CPoint& pt)
{
	auto tray = theApp.m_manifest["tray"];

	if (tray.is_null())
	{
		return;
	}

	auto items = tray["menu"];

	if (items.is_null())
	{
		return;
	}

	CMenu trayMenu;
	trayMenu.CreateMenu();

	DWORD cmdOffset = 0;
	vector<CMenu*> menusToDelete;

	AppendMenuItems(&trayMenu, 0, cmdOffset, _T("&Menu"), items, menusToDelete);

	SetForegroundWindow();
	
	CMenu* pSubMenu = trayMenu.GetSubMenu(0);
	pSubMenu->TrackPopupMenu(TPM_BOTTOMALIGN | TPM_LEFTBUTTON | TPM_RIGHTBUTTON, pt.x, pt.y, this);

	PostMessage(WM_NULL);

	for (auto pMenu : menusToDelete)
	{
		delete pMenu;
	}
}

void CChromeAppPlatformDlg::AppendMenuItems(CMenu* pMenuParent, DWORD position, DWORD& cmdOffset, const CString& menuName, json& items, vector<CMenu*>& menusToDelete)
{
	CMenu* pSubMenu = new CMenu();
	pSubMenu->CreatePopupMenu();

	menusToDelete.push_back(pSubMenu);

	DWORD subMenuPosition = 0;

	for (auto it = items.begin(); it != items.end(); ++it)
	{
		auto item = *it;

		auto nameItem = item["name"];

		if (nameItem.is_null())
		{
			continue;
		}

		CString name(nameItem.get<string>().c_str());

		auto subMenu = item["menu"];

		if (!subMenu.is_null())
		{
			AppendMenuItems(pSubMenu, subMenuPosition, cmdOffset, name, subMenu, menusToDelete);
		}
		else
		{
			UINT_PTR menuItemID = WM_TRAY_START + cmdOffset++;

			auto callback = item["callback"];
			
			if (!callback.is_null())
			{
				CString itemCallback(callback.get<string>().c_str());
				m_trayCommands[menuItemID] = itemCallback;
			}

			auto flags = MF_STRING;
			pSubMenu->AppendMenu(flags, menuItemID, name);
		}

		subMenuPosition++;
	}

	pMenuParent->InsertMenu(position, MF_BYPOSITION | MF_POPUP, (UINT_PTR)pSubMenu->m_hMenu, menuName);
}

void CChromeAppPlatformDlg::OnTrayClose()
{
	KillTimer(TIMER_UPDATE);
	m_browserInterface.PrepareToClose();
}

void CChromeAppPlatformDlg::RunExternalApp()
{
	auto extAppItem = theApp.m_manifest["externalApp"];

	if (extAppItem.is_null())
	{
		return;
	}

	auto appPathItem = extAppItem["path"];

	if (appPathItem.is_null())
	{
		return;
	}

	CString path(appPathItem.get<string>().c_str());

	auto appParamsItem = extAppItem["params"];

	CString params;

	if (!appParamsItem.is_null())
	{
		params = appParamsItem.get<string>().c_str();
	}

	auto showItem = extAppItem["show"];

	bool isShowWindow = false;

	if (!showItem.is_null())
	{
		isShowWindow = showItem;
	}

	auto redirectOutputItem = extAppItem["redirectOutput"];

	bool isRedirectOutput = false;

	if (!redirectOutputItem.is_null())
	{
		isRedirectOutput = redirectOutputItem;
	}

	shared_ptr<CExternalProcess> pExtProcess(new CExternalProcess);

	STARTUPINFO startupInfo = { 0 };

	startupInfo.cb = sizeof(startupInfo);
	startupInfo.wShowWindow = isShowWindow ? SW_SHOW : SW_HIDE;

	if (isRedirectOutput)
	{
		SECURITY_ATTRIBUTES securityAttr = { 0 };
		securityAttr.nLength = sizeof(SECURITY_ATTRIBUTES);

		securityAttr.bInheritHandle = TRUE;
		securityAttr.lpSecurityDescriptor = NULL;

		if (!CreatePipe(&pExtProcess->m_stdOutRead, &pExtProcess->m_stdOutWrite, &securityAttr, 0))
		{
			return;
		}

		if (!SetHandleInformation(pExtProcess->m_stdOutRead, HANDLE_FLAG_INHERIT, 0))
		{
			return;
		}

		startupInfo.hStdOutput = pExtProcess->m_stdOutWrite;
		startupInfo.dwFlags |= STARTF_USESTDHANDLES;

		auto stdoutCallbackItem = extAppItem["stdOutCallback"];

		if (!stdoutCallbackItem.is_null())
		{
			m_stdOutCallback = stdoutCallbackItem.get<string>().c_str();
		}
	}

	PROCESS_INFORMATION processInfo = { 0 };

	CreateProcess(NULL, path.GetBuffer(), NULL, NULL, TRUE, NULL, NULL, NULL, &startupInfo, &processInfo);
	path.ReleaseBuffer();

	auto callbackItem = extAppItem["launchCallback"];
	
	if (callbackItem.is_null())
	{
		return;
	}

	CString callback(callbackItem.get<string>().c_str());
	m_browserInterface.ExecuteJavaScript(callback);

	pExtProcess->m_processHandle = processInfo.hProcess;

	MonitorProcess(pExtProcess);
	CloseHandle(processInfo.hThread);
}

void CChromeAppPlatformDlg::OutputRedirectThread(CChromeAppPlatformDlg* pDlg, HANDLE stdOutRead)
{
	DWORD bytesRead = 0;
	CStringA outputBuffer;
	
	for (;;)
	{
		BOOL isSuccess = ReadFile(stdOutRead, outputBuffer.GetBuffer(STD_OUT_BUFFER_SIZE), STD_OUT_BUFFER_SIZE, &bytesRead, NULL);
		outputBuffer.ReleaseBuffer(bytesRead);

		if (!isSuccess || bytesRead == 0)
		{
			break;
		}

		pDlg->SendMessage(WM_EXPROCESS_OUTPUT, NULL, (LPARAM) &outputBuffer);
	}
}

void CChromeAppPlatformDlg::MonitorProcessThread(CChromeAppPlatformDlg* pDlg, shared_ptr<CExternalProcess> pExtProcess)
{
	WaitForSingleObject(pExtProcess->m_processHandle, INFINITE);
	
	CloseHandle(pExtProcess->m_processHandle);
	CloseHandle(pExtProcess->m_stdOutWrite);
	CloseHandle(pExtProcess->m_stdOutRead);

	pDlg->PostMessage(WM_PROCESS_CLOSED, (WPARAM) pExtProcess->m_processHandle);
}

void CChromeAppPlatformDlg::MonitorProcess(shared_ptr<CExternalProcess> pExtProcess)
{
	thread monitorThread(MonitorProcessThread, this, pExtProcess);
	pExtProcess->m_monitorThread.swap(monitorThread);

	if (pExtProcess->m_stdOutRead != NULL)
	{
		thread outputRedirectThread(OutputRedirectThread, this, pExtProcess->m_stdOutRead);
		pExtProcess->m_outputRedirectThread.swap(outputRedirectThread);
	}

	m_procMonitorThreads[pExtProcess->m_processHandle] = pExtProcess;
}

LRESULT CChromeAppPlatformDlg::OnProcessClosed(WPARAM wParam, LPARAM lParam)
{
	HANDLE process = (HANDLE) wParam;

	auto it = m_procMonitorThreads.find(process);

	if (it == m_procMonitorThreads.end())
	{
		return 0;
	}

	shared_ptr<CExternalProcess> pExtProcess = it->second;

	thread& monitorThread = pExtProcess->m_monitorThread;

	if (monitorThread.joinable())
	{
		monitorThread.join();
	}

	thread& outputRedirectThread = pExtProcess->m_outputRedirectThread;
	
	if (outputRedirectThread.joinable())
	{
		outputRedirectThread.join();
	}

	m_procMonitorThreads.erase(it);

	auto extAppItem = theApp.m_manifest["externalApp"];

	if (extAppItem.is_null())
	{
		return 0;
	}

	auto callbackItem = extAppItem["closeCallback"];

	if (!callbackItem.is_null())
	{
		CString callback(callbackItem.get<string>().c_str());
		m_browserInterface.ExecuteJavaScript(callback);
	}

	return 0;
}

LRESULT CChromeAppPlatformDlg::OnExtProcessOutput(WPARAM wParam, LPARAM lParam)
{
	CStringA* pOutputBuffer = (CStringA*) lParam;

	if (pOutputBuffer == NULL)
	{
		return 0;
	}

	if (m_stdOutCallback.IsEmpty())
	{
		return 0;
	}

	CString jsCode;
	jsCode.Format(_T("%s(`%S`)"), m_stdOutCallback, *pOutputBuffer);

	m_browserInterface.ExecuteJavaScript(jsCode);
	return 0;
}
