// ChromeAppPlatformDlg.h : header file
//

#pragma once

#include "BrowserInterface.h"

class CChromeAppPlatformDlg : public CDialogEx
{
public:
	CChromeAppPlatformDlg(CWnd* pParent = NULL);
	~CChromeAppPlatformDlg();

#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CHROMEAPPPLATFORM_DIALOG };
#endif

protected:
	virtual void OnClose();
	
	virtual void OnTimer(UINT_PTR uIDEvent);
	virtual void OnSize(UINT nType, int cx, int cy);
	virtual void DoDataExchange(CDataExchange* pDX);

	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()

private:
	bool m_isTopMost;
	bool m_isOverlayWindow;

	HICON m_iconHandle;
	HICON m_trayIconHandle;

	HWND m_shellTrayWnd;
	HWND m_cachedForegroundWnd;

	map<UINT_PTR, CString> m_trayCommands;
	map<HANDLE, shared_ptr<CExternalProcess>> m_procMonitorThreads;

	CString m_stdOutCallback;

	CBrowserInterface m_browserInterface;

	void CreateTrayIcon();
	void RunExternalApp();
	void RemoveTrayIcon();
	void SetWindowProperties();
	void ForegroundWindowCheck();
	void ResizeWindowToCoverDesktop();

	void OnTrayClose();
	void OnTrayItemClicked(UINT command);

	void ShowTrayContextMenu(const CPoint& pt);
	
	void MonitorProcess(shared_ptr<CExternalProcess> pExtProcess);

	static void OutputRedirectThread(CChromeAppPlatformDlg* pDlg, HANDLE stdOutRead);
	static void MonitorProcessThread(CChromeAppPlatformDlg* pDlg, shared_ptr<CExternalProcess> pExtProcess);

	void AppendMenuItems(CMenu* pMenuParent, 
		DWORD position, 
		DWORD& cmdOffset, 
		const CString& menuName, 
		json& items,
		vector<CMenu*>& menusToDelete);

	LRESULT OnTrayNotif(WPARAM wParam, LPARAM lParam);
	LRESULT OnProcessClosed(WPARAM wParam, LPARAM lParam);
	LRESULT OnExtProcessOutput(WPARAM wParam, LPARAM lParam);
	LRESULT OnWindowLoadedNotif(WPARAM wParam, LPARAM lParam);
};
