
// ChromeAppPlatform.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"

#include "ChromeAppPlatform.h"
#include "ChromeAppPlatformDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CChromeAppPlatformApp

BEGIN_MESSAGE_MAP(CChromeAppPlatformApp, CWinApp)
END_MESSAGE_MAP()

// CChromeAppPlatformApp construction

CChromeAppPlatformApp::CChromeAppPlatformApp()
{
}

CChromeAppPlatformApp::~CChromeAppPlatformApp()
{
}

// The one and only CChromeAppPlatformApp object

CChromeAppPlatformApp theApp;

// CChromeAppPlatformApp initialization

BOOL CChromeAppPlatformApp::InitInstance()
{
	CoInitialize(NULL);

	if (!CreateTempFolder())
	{
		return FALSE;
	}

	if (!ExtractResources())
	{
		return FALSE;
	}

	InitSettings();
	LoadSettings();

	if (!InitBrowser())
	{
		return FALSE;
	}

	CWinApp::InitInstance();

	CChromeAppPlatformDlg dlg(CWnd::FromHandle(GetDesktopWindow()));
	m_pMainWnd = &dlg;
	
	dlg.DoModal();

	DeleteTempFolder();

	return FALSE;
}

int CChromeAppPlatformApp::ExitInstance()
{
	CefShutdown();
	CoUninitialize();

	return CWinApp::ExitInstance();
}

void CChromeAppPlatformApp::InitSettings()
{
}

void CChromeAppPlatformApp::LoadSettings()
{
	CString manifestPath;
	manifestPath.Format(_T("%s\\manifest.json"), GetModuleFolder());

	CStringA manifestBuffer;

	{
		CFile manifest;

		if (!manifest.Open(manifestPath, CFile::modeRead))
		{
			return;
		}

		int fileSize = (int)manifest.GetLength();

		manifest.Read(manifestBuffer.GetBuffer(fileSize), fileSize);
		manifestBuffer.ReleaseBuffer();
	}

	m_manifest = json::parse(manifestBuffer.GetString());
}

bool CChromeAppPlatformApp::InitBrowser()
{
	CefMainArgs mainargs(m_hInstance);

	m_cefApp = new CBrowserApp();
	CefExecuteProcess(mainargs, m_cefApp, nullptr);

	CefSettings settings;
	settings.multi_threaded_message_loop = true;

#ifdef _DEBUG
	settings.single_process = true;
#endif 

	CString logFilePath;
	logFilePath.Format(_T("%s\\debug.log"), GetTempFolder());

	cef_string_set(logFilePath, logFilePath.GetLength(), &settings.log_file, true);

	if (!CefInitialize(mainargs, settings, m_cefApp, nullptr))
	{
		return false;
	}

	return true;
}

bool CChromeAppPlatformApp::CreateTempFolder()
{
	CString tempFolder;
	
	GetTempPath(MAX_PATH, tempFolder.GetBuffer(MAX_PATH + 1));
	tempFolder.ReleaseBuffer();

	if (tempFolder.IsEmpty())
	{
		return false;
	}

	tempFolder.Append(_T("ChromeAppPlatform"));

	if (!CreateDirectory(tempFolder, NULL))
	{
		if (GetLastError() != ERROR_ALREADY_EXISTS)
		{
			return false;
		}
	}

	m_tempFolder = tempFolder;
	return true;
}

CString CChromeAppPlatformApp::GetConsoleLogPath()
{
	if (!m_consoleLogPath.IsEmpty())
	{
		return m_consoleLogPath;
	}

	m_consoleLogPath.Format(_T("%s\\console.log"), GetTempFolder());
	return m_consoleLogPath;
}

CString CChromeAppPlatformApp::GetModuleFolder()
{
	if (!m_moduleFolder.IsEmpty())
	{
		return m_moduleFolder;
	}

	CString moduleFilename;

	GetModuleFileName(NULL, moduleFilename.GetBuffer(MAX_PATH + 1), MAX_PATH);
	moduleFilename.ReleaseBuffer();
	
	int pos = moduleFilename.ReverseFind(_T('\\'));

	if (pos == -1)
	{
		ASSERT(FALSE);
		return _T("");
	}

	m_moduleFolder = moduleFilename.Left(pos);
	return m_moduleFolder;
}

bool CChromeAppPlatformApp::CopyFileToTemp(const CString& path)
{
	CString source;
	source.Format(_T("%s\\%s"), GetModuleFolder(), path);

	int pos = path.ReverseFind(_T('\\'));

	CString filename;

	if (pos != -1)
	{
		filename = path.Mid(pos + 1);
	}
	else
	{
		filename = path;
	}

	CString destination;
	destination.Format(_T("%s\\%s"), m_tempFolder, filename);

	if (!CopyFile(source, destination, FALSE))
	{
		return false;
	}

	return true;
}

bool CChromeAppPlatformApp::DeleteTempFolder()
{
	TCHAR path[MAX_PATH + 1] = { 0 };
	
	_tcscpy_s(path, m_tempFolder);
	path[m_tempFolder.GetLength() + 1] = 0;

	SHFILEOPSTRUCTW shFileOp = { 0 };

	shFileOp.pFrom = path;
	shFileOp.wFunc = FO_DELETE;
	shFileOp.fFlags = FOF_NO_UI;

	if (SHFileOperation(&shFileOp) != ERROR_SUCCESS)
	{
		return false;
	}

	return true;
}

BOOL CChromeAppPlatformApp::PumpMessage()
{
	auto result = CWinApp::PumpMessage();
	return result;
}

void CChromeAppPlatformApp::WindowLoadedNotif()
{
	m_pMainWnd->SendMessage(WM_WINDOW_LOADED_NOTIF);
}

bool CChromeAppPlatformApp::ExtractResources()
{
	/*map<UINT, CString> resourcesToExtract;

	resourcesToExtract[IDR_JS_APP] = _T("app.js");
	resourcesToExtract[IDR_CSS_STYLE] = _T("style.css");
	resourcesToExtract[IDR_HTML_INDEX] = _T("index.html");

	for (auto resource : resourcesToExtract)
	{
		if (!ExtractResource(resource.first, resource.second))
		{
			return false;
		}
	}*/

	return true;
}

bool CChromeAppPlatformApp::ExtractResource(UINT uID, const CString& filename)
{
	HRSRC resource = FindResource(NULL, MAKEINTRESOURCE(uID), RT_RCDATA);
	
	if (resource == NULL)
	{
		return false;
	}

	DWORD resourceSize = SizeofResource(NULL, resource);
	HGLOBAL resourceData = LoadResource(NULL, resource);

	if (resourceData == NULL)
	{
		return false;
	}

	void* binaryData = ::LockResource(resourceData);

	if (binaryData == nullptr)
	{
		UnlockResource(resourceData);
		return false;
	}

	CString path;
	path.Format(_T("%s\\%s"), m_tempFolder, filename);

	CFile file;
	if (!file.Open(path, CFile::modeCreate | CFile::modeReadWrite))
	{
		UnlockResource(resourceData);
		return false;
	}

	file.Write(binaryData, resourceSize);
	UnlockResource(resourceData);

	return true;
}

