//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ChromeAppPlatform.rc
//
#define IDR_MAINFRAME                   101
#define IDD_CHROMEAPPPLATFORM_DIALOG    102
#define IDR_TRAY_MENU                   130
#define ID_CHROMEAPPPLATFORM_CLOSE      32771
#define ID_CHROMEAPPPLATFORM_APPNAME    32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
