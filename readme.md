# ChromeAppPlatform
------

Launch any web site as a native Windows application in an embedded Chrome frame.

## Manifest File

A manifest file called `manifest.json` has to be created and placed alongside ChromeAppPlatform.exe.

Example of a manifest file:

```
{
    "title": "Default Title",	// Application title. This will be shown in the main dialog title bar and tray tooltip.
    "url": "www.google.com",	// URL to navigate to upon opening the application.
    "opacity": 255              // Value from 0 to 255.
	"width": 800,				// Initial window width.
    "height": 400,				// Initial window height.
    "overlay": false,			// Overlays the window on the desktop.
    "maximized": true,			// Maximizes the application window on launch.
    "icon": "icon.ico",         // Path to an icon to be used in the taskbar (largest size available) and for the tray (16x16).
	"tray": {					// Shows an entry in the system tray.
        "menu": [
            {
                "name": "item 1",					// Menu item name.
                "callback": "OnItem1Clicked"        // Menu item JavaScript callback.
            },
            {
                "name": "sub-menu",
                "menu": [
                    {
                        "name": "sub-menu item 1",
                        "callback": "OnSubItem1Clicked"
                    }
                ]
            },
            {
                "name": "item 2",
                "callback": "OnItem2Clicked"
            }
        ]
    },
    "externalApp": {            // Launch an external process after loading the main page.
        "path": "C:\\Windows\\System32\\ping.exe -t google.com",    // Command line to execute.
        "show": true,                                               // Shows or hides the application window.
        "redirectOutput": true,                                     // Redirects the application stdout.
        "launchCallback": "OnExternalAppLaunched",                  // Callback to be called after launch attempt.      
        "closeCallback": "OnExternalAppClosed"                      // Callback to be called after the process closes.
        "stdOutCallback": "OnStdOutData"                            // Callback to receive stdout data in its first parameter.
    }
}
```
