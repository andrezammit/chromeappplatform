#pragma once

class CBrowserApp : public CefApp
{
public:
	IMPLEMENT_REFCOUNTING(CBrowserApp)
	
	void OnBeforeCommandLineProcessing(const CefString &processType, CefRefPtr<CefCommandLine> commandLine) override;
};

class CEventHandler : public CefClient,
	public CefDisplayHandler,
	public CefLifeSpanHandler,
	public CefLoadHandler
{
public:
	CEventHandler();
	~CEventHandler();

	int m_browserId;
	bool m_isClosing;

	CefRefPtr<CefBrowser> m_pBrowser;

protected:
	IMPLEMENT_REFCOUNTING(CEventHandler);

	virtual CefRefPtr<CefLoadHandler>		GetLoadHandler()		override { return this; }
	virtual CefRefPtr<CefDisplayHandler>	GetDisplayHandler()		override { return this; }
	virtual CefRefPtr<CefLifeSpanHandler>	GetLifeSpanHandler()	override { return this; }

	virtual void CEventHandler::OnAfterCreated(CefRefPtr<CefBrowser> browser) override;

	virtual void OnLoadEnd(CefRefPtr<CefBrowser> browser,
		CefRefPtr<CefFrame> frame,
		int httpStatus) override;

	virtual void OnLoadError(CefRefPtr<CefBrowser> browser,
		CefRefPtr<CefFrame> frame,
		cef_errorcode_t errorCode,
		const CefString& errorText,
		const CefString& failedUrl) override;

	virtual void OnBeforeClose(CefRefPtr<CefBrowser> browser) override;
	virtual void OnLoadingStateChange(CefRefPtr<CefBrowser> browser, bool isLoading, bool canGoBack, bool canGoForward) override;

	virtual bool DoClose(CefRefPtr<CefBrowser> browser) override;
	virtual bool OnConsoleMessage(CefRefPtr<CefBrowser> browser, const CefString& message, const CefString& source, int line);
};
