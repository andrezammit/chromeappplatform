function OnItem1Clicked() {
    appendEventText("OnItem1Clicked");
}

function OnSubItem1Clicked() {
    appendEventText("OnSubItem1Clicked");
}

function OnItem2Clicked() {
    appendEventText("OnItem2Clicked");
}

function OnExternalAppLaunched() {
    appendEventText("OnExternalAppLaunched");
}

function OnExternalAppClosed() {
    appendEventText("OnExternalAppClosed");
}

function appendEventText(text) {
    var eventsNode = document.querySelector("#events");
    eventsNode.textContent += text + "\r\n";
}

function OnStdOutData(text) {
    var stdOutNode = document.querySelector("#stdOut");
    stdOutNode.textContent += text;
}
