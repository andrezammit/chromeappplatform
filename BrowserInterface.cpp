#include "stdafx.h"

#include "ChromeAppPlatform.h"
#include "BrowserInterface.h"

CBrowserInterface::CBrowserInterface()
{
	m_pEventHandler = nullptr;
}

CBrowserInterface::~CBrowserInterface()
{
}

bool CBrowserInterface::Create(HWND hWnd, CRect& rect, const CStringA& url)
{
	CefWindowInfo info;
	info.SetAsChild(hWnd, rect);

	m_pEventHandler = new CEventHandler();
	
	CefBrowserSettings browserSettings;
	if (!CefBrowserHost::CreateBrowser(info, m_pEventHandler.get(), url.GetString(), browserSettings, nullptr))
	{
		return false;
	}

	return true;
}

void CBrowserInterface::UpdateSize(CRect& rect)
{
	if (m_pEventHandler == nullptr)
	{
		return;
	}

	auto pBrowser = m_pEventHandler->m_pBrowser;

	if (pBrowser == nullptr)
	{
		return;
	}

	auto pBrowserHost = pBrowser->GetHost();
	auto hWnd = pBrowserHost->GetWindowHandle();

	::SetWindowPos(hWnd, HWND_BOTTOM, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_NOZORDER);
}

bool CBrowserInterface::ExecuteJavaScript(CString jsCode)
{
	if (m_pEventHandler == nullptr)
	{
		return false;
	}

	auto pBrowser = m_pEventHandler->m_pBrowser;

	if (pBrowser == NULL)
	{
		return false;
	}

	auto pFrame = pBrowser->GetMainFrame();

	if (pFrame == NULL)
	{
		return false;
	}

	if (jsCode.Right(0) != _T(")"))
	{
		jsCode.Append(_T("()"));
	}

	pFrame->ExecuteJavaScript(jsCode.GetString(), pFrame->GetURL(), 0);
	return true;
}

CString CBrowserInterface::EscapeString(const CString& string)
{
	CString escaped = string;
	escaped.Replace(_T("'"), _T("\\'"));

	return escaped;
}

bool CBrowserInterface::IsReadyToClose()
{
	if (m_pEventHandler == nullptr)
	{
		return true;
	}
	
	auto pBrowser = m_pEventHandler->m_pBrowser;

	if (pBrowser == NULL)
	{
		return true;
	}

	if (m_pEventHandler->m_isClosing)
	{
		return true;
	}

	return false;
}

void CBrowserInterface::PrepareToClose()
{
	if (m_pEventHandler == nullptr)
	{
		return;
	}

	auto pBrowser = m_pEventHandler->m_pBrowser;

	if (pBrowser == nullptr)
	{
		return;
	}
	
	auto pBrowserHost = pBrowser->GetHost();

	if (pBrowserHost == nullptr)
	{
		return;
	}

	pBrowserHost->CloseBrowser(true);
}

void CBrowserInterface::SetMousePosition(int x, int y)
{
	CefMouseEvent mouseEvent;

	mouseEvent.x = x;
	mouseEvent.y = y;

	if (m_pEventHandler == nullptr)
	{
		return;
	}

	auto pBrowser = m_pEventHandler->m_pBrowser;

	if (pBrowser == NULL)
	{
		return;
	}

	auto pBrowserHost = pBrowser->GetHost();

	if (pBrowserHost == nullptr)
	{
		return;
	}

	pBrowserHost->SendMouseMoveEvent(mouseEvent, false);
}
