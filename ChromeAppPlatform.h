
// ChromeAppPlatform.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols
#include "EventHandler.h"

#define WM_TRAY_NOTIF				WM_USER + 101
#define WM_WINDOW_LOADED_NOTIF		WM_USER + 102
#define WM_PROCESS_CLOSED			WM_USER + 103
#define WM_EXPROCESS_OUTPUT			WM_USER + 104

#define WM_TRAY_START				WM_USER + 200
#define WM_TRAY_END					WM_USER + 299

class CExternalProcess
{
public:
	CExternalProcess()
	{
		m_processId = 0;
		m_processHandle = NULL;

		m_stdOutRead = NULL;
		m_stdOutWrite = NULL;
	}

	~CExternalProcess()
	{
	}

	DWORD m_processId;
	HANDLE m_processHandle;

	HANDLE m_stdOutRead;
	HANDLE m_stdOutWrite;

	thread m_monitorThread;
	thread m_outputRedirectThread;
};

class CChromeAppPlatformApp : public CWinApp
{
public:
	CChromeAppPlatformApp();
	~CChromeAppPlatformApp();

	json m_manifest;

	void WindowLoadedNotif();

	CString GetModuleFolder();
	CString GetConsoleLogPath();

	CString GetTempFolder() { return m_tempFolder; }

protected:
	virtual BOOL InitInstance();
	
	int ExitInstance();

	DECLARE_MESSAGE_MAP()

private:
	CString m_tempFolder;
	CString m_moduleFolder;

	CString m_consoleLogPath;

	CefRefPtr<CBrowserApp> m_cefApp;

	void InitSettings();
	void LoadSettings();

	bool InitBrowser();
	bool CreateTempFolder();
	bool DeleteTempFolder();
	bool ExtractResources();

	bool CopyFileToTemp(const CString& path);
	bool ExtractResource(UINT uID, const CString& filename);

	BOOL PumpMessage();
};

extern CChromeAppPlatformApp theApp;
