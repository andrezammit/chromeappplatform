#pragma once

#include "EventHandler.h"

class CBrowserInterface
{
public:
	CBrowserInterface();
	~CBrowserInterface();

	void PrepareToClose();
	
	void UpdateSize(CRect& rect);
	void SetMousePosition(int x, int y);

	bool IsReadyToClose();

	bool ExecuteJavaScript(CString jsCode);
	bool Create(HWND hWnd, CRect& rect, const CStringA& url);

	static CString EscapeString(const CString& string);

private:
	CefRefPtr<CEventHandler> m_pEventHandler;
};

