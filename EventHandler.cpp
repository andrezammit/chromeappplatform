#include "stdafx.h"

#include "ChromeAppPlatform.h"
#include "EventHandler.h"

void CBrowserApp::OnBeforeCommandLineProcessing(const CefString &processType, CefRefPtr<CefCommandLine> commandLine)
{
	commandLine->AppendSwitch(_T("enable-media-stream"));
}

CEventHandler::CEventHandler()
{
	m_browserId = 0;
	m_isClosing = false;
}

CEventHandler::~CEventHandler()
{
}

void CEventHandler::OnLoadEnd(CefRefPtr<CefBrowser> browser,
	CefRefPtr<CefFrame> frame,
	int httpStatus)
{
	CEF_REQUIRE_UI_THREAD();

	theApp.WindowLoadedNotif();
}

bool CEventHandler::OnConsoleMessage(CefRefPtr<CefBrowser> browser, const CefString& message, const CefString& source, int line)
{
	unique_ptr<CString> pMessage(new CString(message.c_str()));
	pMessage->Append(_T("\n"));

	CFile consoleLog;
	if (consoleLog.Open(theApp.GetConsoleLogPath(), CFile::modeNoTruncate | CFile::modeWrite | CFile::modeCreate))
	{
		CStringA ansiMessage(*pMessage);

		consoleLog.SeekToEnd();
		consoleLog.Write(ansiMessage, ansiMessage.GetLength());
	}

	TRACE(_T("%s"), *pMessage);
	return false;
}

void CEventHandler::OnLoadError(CefRefPtr<CefBrowser> browser,
	CefRefPtr<CefFrame> frame,
	cef_errorcode_t errorCode,
	const CefString& errorText,
	const CefString& failedUrl)
{
	CEF_REQUIRE_UI_THREAD();
}

void CEventHandler::OnLoadingStateChange(CefRefPtr<CefBrowser> browser, bool isLoading, bool canGoBack, bool canGoForward)
{
	CEF_REQUIRE_UI_THREAD();
}

void CEventHandler::OnAfterCreated(CefRefPtr<CefBrowser> browser) 
{
	CEF_REQUIRE_UI_THREAD();

	if (m_pBrowser.get())
	{
		ASSERT(FALSE);
		return;
	}

	m_pBrowser = browser;
	m_browserId = browser->GetIdentifier();
}

bool CEventHandler::DoClose(CefRefPtr<CefBrowser> browser) 
{
	CEF_REQUIRE_UI_THREAD();

	ASSERT(m_browserId == browser->GetIdentifier());

	m_isClosing = true;
	return false;
}

void CEventHandler::OnBeforeClose(CefRefPtr<CefBrowser> browser) 
{
	CEF_REQUIRE_UI_THREAD();

	ASSERT(m_browserId == browser->GetIdentifier());

	m_pBrowser = nullptr;
}
